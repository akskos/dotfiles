" Section: Bootstrap

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'mfukar/robotframework-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'leafgarland/typescript-vim'
Plugin 'wikitopian/hardmode'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'Valloric/YouCompleteMe'
Plugin 'tpope/vim-commentary'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Section: Maps

imap jj <Esc>
nmap ö :
nmap ä ^
vmap ä ^
nmap _ :NERDTree<CR>
nmap Q <nop>
nnoremap <CR> :pclose<CR>:noh<CR>
nmap <C-p> :CtrlP<CR>
nmap J V:sleep 200m<CR><Esc>
nmap K va{$o0d

" Section: Vim settings

" Settings
set number
set relativenumber
set tabstop=2
set shiftwidth=2
set expandtab
set ignorecase
set smartcase
set incsearch
set splitbelow

" Section: Plugin settings

let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_extensions = ['line']
let g:ycm_enable_diagnostic_signs = 0

" Set better colors
hi Comment ctermfg=lightblue
hi Search ctermbg=220

" Functions
function! IsNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction
function! SyncTree()
  if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && expand('%') !~# 'NERD_tree_[0-9]\+' && !&diff
    NERDTreeFind
    wincmd p
  endif
endfunction
function! StartUp()
  if 0 == argc()
    NERDTree
  endif
endfunction 

autocmd BufEnter * call SyncTree()
autocmd VimEnter * call StartUp()

