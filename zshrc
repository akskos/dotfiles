# paths

export EDITOR=/usr/bin/vim
export vimrc=/etc/vimrc
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk
export ANDROID_SDK_ROOT=/opt/android-sdk
export PAGER=less
export docs=/home/akseli/documents
export downl=/home/akseli/downloads
export pics=/home/akseli/pictures
export desk=/home/akseli/desktop
export music=/home/akseli/music
export videos=/home/akseli/videos
export proj=/home/akseli/projects
export esp=/boot
export PATH=$PATH:/usr/local/bin:/home/akseli/bin:$HOME/tiny-tools
export PATH=$PATH:/home/akseli/.gem/ruby/2.1.0/bin
export NODE_PATH=/usr/lib/node_modules
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export PATH="$PATH:$HOME/Android/Sdk/platform-tools"
export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin
export home=$HOME

# aliases

alias docs="cd $docs"
alias downl="cd $downl"
alias pics="cd $pics"
alias desk="cd $desk"
alias music="cd $music"
alias vid="cd $videos"
alias proj="cd $proj"
alias ls="ls --color"
alias la="ls -A"
alias ll="ls -lh"
alias lla="ll -A"
alias svim="sudo -E vim"
alias up="cd .."
alias note="$EDITOR $docs/note"
alias sch="feh -. -F $pics/sched.png"
alias vol="pactl set-sink-volume 0"
alias volup="vol +3%"
alias voldown="vol -3%"
alias u=volup
alias d=voldown

# function(s)

function mkcd {
    mkdir -p $1
    cd $1
}

# Searching...
function seekndestroy {
    find . -name "$1" -exec rm -rf {} \;
}

# less settings

export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
export LESS_TERMCAP_md=$(tput bold; tput setaf 9)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 7; tput setab 8)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7)
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)

# other

stty -ixon
autoload -U compinit promptinit colors
compinit
promptinit
prompt walters
colors
export TERM='screen-256color'
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
eval $(thefuck --alias)
